package basilliyc.menucircleanimation

import android.graphics.drawable.Animatable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_app_bar.*
import kotlinx.android.synthetic.main.main_app_menu.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        circle.anchorOnFrameOne = imageViewInToolbar
        imageViewInToolbar.setOnClickListener {
            imageViewInToolbar.postDelayed({ circle.switchFrames() }, 375)

            imageViewInToolbar.setImageDrawable(getDrawable(R.drawable.menu_to_close_primary))
            action_button.setImageDrawable(getDrawable(R.drawable.menu_to_close_white))
            (action_button.drawable as Animatable).start()
            (imageViewInToolbar.drawable as Animatable).start()
        }

        circle.anchorOnFrameTwo= action_button
        action_button.setOnClickListener { closeMenu() }

        val list = listOf("Local", "World", "Region", "Country", "Command", "Movie",
                "Star Wars", "Android", "Monster", "KitKat", "Oreo", "Lolipop")

        recycler.adapter = ItemRecyclerAdapter(list, this) {
            main_text.text = it
            closeMenu()
        }
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.itemAnimator = DefaultItemAnimator()
    }

    fun closeMenu() {
        imageViewInToolbar.postDelayed({
            (action_button.drawable as Animatable).start()
            (imageViewInToolbar.drawable as Animatable).start()
        }, 375)

        imageViewInToolbar.setImageDrawable(getDrawable(R.drawable.close_to_menu_primary))
        action_button.setImageDrawable(getDrawable(R.drawable.close_to_menu_white))
        circle.switchFrames()
    }

}

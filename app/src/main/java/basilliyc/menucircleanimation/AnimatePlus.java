package basilliyc.menucircleanimation;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

public class AnimatePlus extends View {

    private DisplayMetrics metrics;

    private int startColor;
    private int endColor;
    private int animationTime;

    private int width;
    private int height;
    private int animationOffset = 0;

    private Paint paintShader;
    private Paint paintEraserMask;
    private Paint paintMask;
    private Bitmap maskBitmap;
    private Bitmap gradientBitmap;
    private Bitmap resultBitmap;
    private Canvas resultCanvas;

    public AnimatePlus(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        metrics = getResources().getDisplayMetrics();
        setAttrs(attrs);
        init();
    }

    private void setAttrs(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.AnimatePlus);

        startColor = a.getColor(R.styleable.AnimatePlus_plu_start_color, 0xFFAA0000);
        endColor = a.getColor(R.styleable.AnimatePlus_plu_end_color, 0xFF0000AA);
        animationTime = a.getInt(R.styleable.AnimatePlus_plu_animation_time, 1500);

        a.recycle();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        width = w;
        height = h;
        prepareValues();
    }

    private void init() {
        paintShader = new Paint();

        paintEraserMask = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintEraserMask.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

        paintMask = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintMask.setColor(0xFFFFFFFF);
        paintMask.setStyle(Paint.Style.FILL_AND_STROKE);
        paintMask.setStrokeWidth(dpToPx(1));

        ValueAnimator animator = ValueAnimator.ofInt(0, 100);
        animator.setDuration(animationTime);
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                setAnimationStep((int) animation.getCurrentPlayTime());
                invalidate();
            }
        });
        animator.start();
    }

    private void prepareValues() {
        Shader shader = new LinearGradient(0, 0, width, 0, startColor, endColor, Shader.TileMode.MIRROR);
        paintShader.reset();
        paintShader.setAntiAlias(true);
        paintShader.setShader(shader);

        maskBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        maskBitmap.eraseColor(Color.TRANSPARENT);
        Canvas maskCanvas = new Canvas(maskBitmap);
        maskCanvas.drawCircle(height / 2, width / 2, height / 2 - 1, paintMask);

        gradientBitmap = Bitmap.createBitmap(width * 3, height * 3, Bitmap.Config.ARGB_8888);
        Canvas gradientCanvas = new Canvas(gradientBitmap);
        gradientCanvas.drawRect(0, 0, gradientBitmap.getWidth(), gradientBitmap.getHeight(), paintShader);

        resultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        resultCanvas = new Canvas(resultBitmap);

    }

    private void setAnimationStep(int time) {
        double progress = (double) time / (double) animationTime;
        animationOffset = (int) (width * 2 * progress);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        resultCanvas.drawBitmap(gradientBitmap, -width * 2 + animationOffset, -height, null);
        resultCanvas.drawBitmap(maskBitmap, 0, 0, paintEraserMask);
        canvas.drawBitmap(resultBitmap, 0, 0, null);
    }

    private float dpToPx(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

}

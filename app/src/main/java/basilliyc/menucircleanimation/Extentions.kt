package basilliyc.menucircleanimation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

fun Any.log(x: Any? = this) {
    when (x) {
        null -> Log.i("Debug", "null")
        else -> Log.i("Debug", x.toString())
    }
}

fun Any.log(any: Any?, prefix: String? = null, tag: String = this.javaClass.simpleName) {
    Log.i("Debug " + (if (prefix == null) "" else prefix + " ") + tag, any?.toString() ?: "null")
}


fun Any.currentTimeMillis(): Long = Calendar.getInstance().timeInMillis

fun Long.toCalendar() : Calendar {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return calendar
}

fun <T> Activity.goToActivity(clazz:Class<T>, finish: Boolean = false) {
    start(clazz)
    if (finish)
        finish()
}


fun <T> Context.start(clazz:Class<T>) {
    startActivity(Intent(this, clazz))
}

fun Any.toMD5(): String {
    val st = this.toString()
    var digest = ByteArray(0)

    try {
        val messageDigest = MessageDigest.getInstance("MD5")
        messageDigest.reset()
        messageDigest.update(st.toByteArray())
        digest = messageDigest.digest()
    } catch (ignore: NoSuchAlgorithmException) {
    }

    var md5Hex = BigInteger(1, digest).toString(16)

    while (md5Hex.length < 32) {
        md5Hex = "0" + md5Hex
    }

    return md5Hex
}
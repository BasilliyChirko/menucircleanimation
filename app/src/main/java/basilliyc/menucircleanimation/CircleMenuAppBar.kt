package basilliyc.menucircleanimation

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.animation.ValueAnimator
import android.graphics.*
import android.util.TypedValue
import android.view.animation.DecelerateInterpolator


class CircleMenuAppBar(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    val animationTime = 600L

    var progress = 0f

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    val frameOne: View
        get() = getChildAt(0)

    val frameTwo: View
        get() = getChildAt(1)

    var frameOneIsShow = true


    lateinit var anchorOnFrameOne: View
    lateinit var anchorOnFrameTwo: View

    val paintEraserMask: Paint
    val paintMask: Paint
    val paintEraser: Paint

    lateinit var bitmapFrameTwo: Bitmap
    lateinit var canvasFrameTwo: Canvas

    lateinit var bitmapMask: Bitmap
    lateinit var canvasMask: Canvas

    var isBitmapPrepare = false

    init {
        paint.color = 0xFFFF4466.toInt()

        paintEraserMask = Paint(Paint.ANTI_ALIAS_FLAG)
        paintEraserMask.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_IN)

        paintEraser = Paint(Paint.ANTI_ALIAS_FLAG)
        paintEraser.color = Color.TRANSPARENT
        paintEraser.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)

        paintMask = Paint(Paint.ANTI_ALIAS_FLAG)
        paintMask.color = 0xFFFFFFFF.toInt()
        paintMask.style = Paint.Style.FILL

        setWillNotDraw(false)
    }


    fun prepareBitmaps() {
        bitmapFrameTwo = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        canvasFrameTwo= Canvas(bitmapFrameTwo)

        bitmapMask= Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        canvasMask = Canvas(bitmapMask)
    }


    fun switchFrames() {
        frameOneIsShow = !frameOneIsShow

        val from = if (frameOneIsShow) 1f else 0f
        val to = if (frameOneIsShow) 0f else 1f


        val animator = ValueAnimator.ofFloat(from, to)
        animator.duration = animationTime
        animator.interpolator = DecelerateInterpolator()
        animator.addUpdateListener {
            progress = it.animatedValue as Float
            invalidate()
        }
        animator.start()


    }


    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)

        if (!isBitmapPrepare) prepareBitmaps()

        if (!frameOneIsShow && progress == 0f || frameOneIsShow && progress == 1f ) {
            frameOne.visibility = View.VISIBLE
            frameTwo.visibility = View.VISIBLE
        }

        if (!frameOneIsShow && progress == 1f || frameOneIsShow && progress == 0f ) {
            frameOne.visibility = if (!frameOneIsShow) View.GONE else View.VISIBLE
            frameTwo.visibility = if (frameOneIsShow) View.GONE else View.VISIBLE
            return
        }

        frameOne.draw(canvas)

        val size = Math.sqrt(Math.pow(width.toDouble(), 2.0) + Math.pow(height.toDouble(), 2.0))


        val centerX =
                if (frameOneIsShow) anchorOnFrameOne.x + anchorOnFrameOne.width / 2
                else anchorOnFrameTwo.x + anchorOnFrameTwo.width / 2
        val centerY =
                if (frameOneIsShow) anchorOnFrameOne.y + anchorOnFrameOne.height / 2
                else anchorOnFrameTwo.y + anchorOnFrameTwo.height / 2


        canvasMask.drawCircle(centerX, centerY, size.toFloat() * progress, paintMask)

        frameTwo.draw(canvasFrameTwo)
        canvasFrameTwo.drawBitmap(bitmapMask, 0f, 0f, paintEraserMask)

        canvas.drawBitmap(bitmapFrameTwo, 0f, 0f, Paint())

    }

    fun Int.toDp(): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), resources.displayMetrics)
    }


}
package basilliyc.menucircleanimation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.recycler_item.view.*

class ItemRecyclerHolder(var view: View) : RecyclerView.ViewHolder(view) {
    var text: TextView = view.text
}

class ItemRecyclerAdapter(var data: List<String>, var context: Context, var click: (String) -> Unit) : RecyclerView.Adapter<ItemRecyclerHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemRecyclerHolder {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return ItemRecyclerHolder(inflater.inflate(R.layout.recycler_item, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ItemRecyclerHolder, position: Int) {
        holder.text.text = data[position]
        holder.view.setOnClickListener { click(data[position]) }
    }


}